from flask import Flask

# Import Facebook
from facebook import Facebook

# Instanciando Clases
facebook = Facebook()

app = Flask(__name__)

@app.route('/validate-webhook')
def validateWebhook():
    return facebook.validate()

@app.route('/validate-webhook', methods=['POST'])
def webhook():
    return facebook.receivedMessage()

@app.route('/send-message')
def sendMessage():
    return facebook.sendMessage()

@app.route('/send-actions')
def sendActions():
    return facebook.sendActions()

@app.route('/send-quick-replies')
def sendQuickReplies():
    return facebook.sendQuickReplies()

if __name__ == '__main__':
    app.run(port=8089, debug=True)
