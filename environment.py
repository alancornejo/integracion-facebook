import os

from dotenv import load_dotenv
load_dotenv()


class Environment():

    def facebook(self):
        return {
            'TOKEN': os.getenv("TOKEN", "NONE"),
            'ACCESS_TOKEN': os.getenv("ACCESS_TOKEN", "NONE"),
            'URL_API_GRAPH': os.getenv("URL_API_GRAPH", "NONE")
        }

    def twilio(self):
        pass
