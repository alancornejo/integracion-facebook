import requests
import json
from environment import Environment as Env


class SendRequests(Env):
    def __init__(self):
        # Almacenamos las variables env de conexión a Facebook
        envFacebook = self.facebook()
        print(envFacebook)
        self.ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URL_API_GRAPH = envFacebook['URL_API_GRAPH']

        # Configuramos los parametros generales para el envió de información hacia Facebook
        self.params = {
            "access_token": self.ACCESS_TOKEN
        }

        self. headers = {
            "Content-Type": "application/json"
        }

    def get(self):
        pass

    def post(self, url, data):
        response = requests.post(
            f'{self.URL_API_GRAPH}/{url}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            self.log(response.status_code)
            self.log(response.text)
        else:
            return response.text

    def log(self, message):  # funcion de logging para heroku
        print('LOGGER')
        print(json.dumps(message, indent=2))
