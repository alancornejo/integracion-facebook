from flask import request

from environment import Environment as Env
from sendRequests import SendRequests

import json


class Facebook(Env):

    def __init__(self):
        envFacebook = self.facebook()
        self.TOKEN = envFacebook['TOKEN']

    def logFacebook(self, data):
        # Para imprimir el log en consola
        print('LOG FACEBOOK')
        print(json.dumps(data, indent=2))

    def validate(self):
        # Almacenamos los valores enviados como parametros en el request
        valueHubMode = request.args.get("hub.mode")
        valueHubChallenge = request.args.get("hub.challenge")
        valueHubToken = request.args.get("hub.verify_token")

        if valueHubMode == "subscribe" and valueHubChallenge:
            # Validar que el Token enviando por facebook sea idéntico al que tenemos en nuestro servidor
            # Si es invalido devolvemos un status code 403
            # Caso contrario devolvemos un status code 200 con el valor hub.challenge
            if not valueHubToken == self.TOKEN:
                return "Verification token mismatch", 403

            return valueHubChallenge, 200

    def sendMessage(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()
        data = {
            "recipient": {"id": data['recipientID']},
            "message": {"text": data['messageText']}
        }

        # instanciamos la clase SendRequests
        sendRequests = SendRequests()
        return sendRequests.post('me/messages', data)

    def receivedMessage(self):
        self.logFacebook(request.get_json())
        return 'ok', 200

    def sendActions(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()
        data = {
            "recipient": {"id": data['recipientID']},
            "sender_action": data['sender_action']
        }

        # instanciamos la clase SendRequests
        sendRequests = SendRequests()
        return sendRequests.post('me/messages', data)

    def sendQuickReplies(self):
        data = request.get_json()
        data = {
            "recipient": {"id": data['recipientID']},
            "messaging_type": "RESPONSE",
            "message": {
                "text": "Seleccione un color:",
                "quick_replies": [{
                    "content_type": "text",
                    "title": "Red",
                    "payload": "circle_red",
                    "image_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Frpp.pe%2Fvirales%2Fmas-virales%2Fpuedes-ver-las-tres-imagenes-ocultas-en-este-circulo-rojo-noticia-955223&psig=AOvVaw0mBoAjy7qb39aOHg7tkzw_&ust=1582234342686000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOih-bHI3ucCFQAAAAAdAAAAABAD"
                }, {
                    "content_type": "text",
                    "title": "Green",
                    "payload": "circle_green",
                    "image_url": "https://images.emojiterra.com/google/android-10/512px/1f7e2.png"
                }]
            }
        }

        # instanciamos la clase SendRequests
        sendRequests = SendRequests()
        return sendRequests.post('me/messages', data)
